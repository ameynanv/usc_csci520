/*

  USC/Viterbi/Computer Science
  "Jello Cube" Assignment 1 starter code

*/

#include "input.h"
#include <iostream>

#include "particleSystem.h"
using namespace std;


/* Write a screenshot to the specified filename, in PPM format */
void saveScreenshot (int windowWidth, int windowHeight, char *filename) {
	  if (filename == NULL)
	    return;

	  // Allocate a picture buffer
	  Pic * in = pic_alloc(windowWidth, windowHeight, 3, NULL);

	  printf("File to save to: %s\n", filename);

	  for (int i=windowHeight-1; i>=0; i--)
	  {
	    glReadPixels(0, windowHeight-i-1, windowWidth, 1, GL_RGB, GL_UNSIGNED_BYTE,
	      &in->pix[i*in->nx*in->bpp]);
	  }

	  if (ppm_write(filename, in))
	    printf("File saved Successfully\n");
	  else
	    printf("Error in Saving\n");

	  pic_free(in);
}


/* converts mouse drags into information about rotation/translation/scaling */
void mouseMotionDrag(int x, int y)
{
  int vMouseDelta[2] = {x-g_vMousePos[0], y-g_vMousePos[1]};

  if (g_iRightMouseButton) // handle camera rotations
  {
    Phi += vMouseDelta[0] * 0.01;
    Theta += vMouseDelta[1] * 0.01;

    if (Phi>2*pi)
      Phi -= 2*pi;

    if (Phi<0)
      Phi += 2*pi;

    if (Theta>pi / 2 - 0.01) // dont let the point enter the north pole
      Theta = pi / 2 - 0.01;

    if (Theta<- pi / 2 + 0.01)
      Theta = -pi / 2 + 0.01;

    g_vMousePos[0] = x;
    g_vMousePos[1] = y;
  }
}

void mouseMotion (int x, int y)
{
  g_vMousePos[0] = x;
  g_vMousePos[1] = y;
}

void mouseButton(int button, int state, int x, int y)
{
  switch (button)
  {
    case GLUT_LEFT_BUTTON:
    	g_iLeftMouseButtonDown = (state==GLUT_DOWN);
    	g_iLeftMouseButtonUp = (state==GLUT_UP);
		if (g_iLeftMouseButtonDown) {
		    mouseOrigin[0] = x;
		    mouseOrigin[1] = y;
		}
      break;
    case GLUT_MIDDLE_BUTTON:
      g_iMiddleMouseButton = (state==GLUT_DOWN);
      break;
    case GLUT_RIGHT_BUTTON:
      g_iRightMouseButton = (state==GLUT_DOWN);
      break;
  }

  g_vMousePos[0] = x;
  g_vMousePos[1] = y;
}


void keyboardFunc (unsigned char key, int x, int y) {
	 switch (key)
	  {
	    case 27:
	      logFile.close();
	      exit(0);
	      break;
	    case 'p':
	      pause = 1 - pause;
	      break;
	    case 'z':
	      R -= 0.2;
	      if (R < 0.2)
	        R = 0.2;
	      break;
	    case 'x':
	      R += 0.2;
	      break;
	    case 'f':
	      externalForce = 1 - externalForce;
	      extHoriAcce = 0.0;
	      extVertiAcce = 0.0;
	      break;
	    case 't':
	      timeVaryingField = 1 - timeVaryingField;
	      extHoriAcce = 0.0;
	      extVertiAcce = 0.0;
	      break;
	    case 'a':
	      if (externalForce) {
	    	  if (extHoriAcce > 0) extHoriAcce = 0.0;
	    	  extHoriAcce -= 0.5;
	      }
	      break;
	    case 'd':
	      if (externalForce) {
	    	  extHoriAcce += 0.5;
	      }
	      break;
	    case 'w':
	      if (externalForce) {
	    	  extVertiAcce += 0.5;
	      }
	      break;
	    case 's':
	      if (externalForce) {
	    	  extVertiAcce -= 0.5;
	      }
	      break;
	    case ' ':
	      saveScreenToFile = 1 - saveScreenToFile;
	      break;
	    default:
	      printf("Key = %d\n", key);
	  }
}

void createWorld(struct world* ps, int argc, char ** argv) {
	if (argc < 5) {
	    printf ("Incorrect parameters to run Particle System Simulation\n");
	    printf ("Usage: %s [NumberOfParticles] [Euler|RK4] [beta] [alpha]\n", argv[0]);
	    printf ("Sample Usage: %s 11 Euler 2.5 0.2 \n", argv[0]);
	    exit(0);
	}
	ps->N = atoi(argv[1]);
	strcpy(ps->integrator,argv[2]);
	ps->beta = atof(argv[3]);
	ps->alpha = atof(argv[4]);
	ps->n = 20;
	ps->ringDiameter = 1.0;
	ps->mass = 1.0/(ps->N*1.0);
	ps->dt = 0.002;

	ps->q = (double *) malloc(sizeof(double) * ps->N * 2);
	ps->q_dot = (double *) malloc(sizeof(double) * ps->N * 2);
	ps->force = (double *) malloc(sizeof(double) * ps->N * 2);
	if (cRingConstraint) {
		for (int i = 0 ; i < ps->N / 2; i++) {
			// Setup initial positions
			ps->q[2*i] = 0.0;
			ps->q[2*i+1] = ps->ringDiameter/2.0 - (i*ps->ringDiameter)/((ps->N-1)*1.0);

			// Setup initial velocities
			ps->q_dot[2*i] = 0.0;
			ps->q_dot[2*i+1] = 0.0;

			// Setup initial forces (gravity)
			ps->force[2*i] = 0.0;
			ps->force[2*i+1] = -1.0 * ps->mass;
		}
		for (int i = ps->N / 2; i < ps-> N; i++) {
			// Setup initial positions
			ps->q[2*i] = (i*ps->ringDiameter)/((ps->N-1)*1.0) - ps->ringDiameter/2.0;
			ps->q[2*i+1] = 0.0;

			// Setup initial velocities
			ps->q_dot[2*i] = 0.0;
			ps->q_dot[2*i+1] = 0.0;

			// Setup initial forces (gravity)
			ps->force[2*i] = 0.0;
			ps->force[2*i+1] = -1.0 * ps->mass;
		}

	} else {
		for (int i = 0 ; i < ps->N; i++) {
			// Setup initial positions
			ps->q[2*i] = (i*ps->ringDiameter)/((ps->N-1)*1.0);
			ps->q[2*i+1] = ps->ringDiameter/2.0;

			// Setup initial velocities
			ps->q_dot[2*i] = 0.0;
			ps->q_dot[2*i+1] = 0.0;

			// Setup initial forces (gravity)
			ps->force[2*i] = 0.0;
			ps->force[2*i+1] = -1.0 * ps->mass;
		}
	}
}
