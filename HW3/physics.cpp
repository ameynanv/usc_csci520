#include "physics.h"
#include <iostream>
#include "particleSystem.h"

using namespace std;

double SQ(double num) {return num*num;}
int XPOS(int num) {return 2*num;}
int YPOS(int num) {return (2*num+1);}

void printDoubleArray(double * A, int rowSize, int colSize) {
	printf("**********\n");
	for (int row = 0; row < rowSize; row++) {
		for (int col = 0; col < colSize; col++) {
			int index = row * colSize + col;
			printf("%.2f\t", A[index]);
		}
		printf("\n");
	}
	printf("**********\n");
}


void computeMatrix_C(struct world *ps, double * C) {
	int N = ps->N;
	double l = ps->ringDiameter / ((N - 1) * 1.0); // length between particles
	for (int i = 0; i< N + 2; i++) {
		if (i == 0) {
			C[i] = ps->q[0];
		} else if (i == 1) {
			C[i] = ps->q[1] - ps->ringDiameter/2.0;
		} else if (i == N + 1) {
			if (cRingConstraint) {
				C[i] = SQ(ps->q[XPOS(N-1)])
						+ SQ(ps->q[YPOS(N-1)])
						- SQ(ps->ringDiameter/2.0);
			} else {
				C[i] = 0.0;
			}
		} else {
			C[i] = SQ(ps->q[XPOS(i-1)] - ps->q[XPOS(i-2)])
				   + SQ(ps->q[YPOS(i-1)]-ps->q[YPOS(i-2)])
				   - SQ(l);
		}
	}
}

void computeMatrix_Cdot(struct world *ps, double * Cdot) {
	int N = ps->N;
	for (int i = 0; i< N + 2; i++) {
		if (i == 0) {
			Cdot[i] = ps->q_dot[0];
		} else if (i == 1) {
			Cdot[i] = ps->q_dot[1];
		} else if (i == N + 1) {
			if (cRingConstraint) {
				Cdot[i] = 2*ps->q[XPOS(N-1)]*ps->q_dot[XPOS(N-1)]
						+ 2*ps->q[YPOS(N-1)]*ps->q_dot[YPOS(N-1)];
			} else {
				Cdot[i] = 0.0;
			}
		} else {
			Cdot[i] = 2*(ps->q[XPOS(i-1)] - ps->q[XPOS(i-2)]) * (ps->q_dot[XPOS(i-1)] - ps->q_dot[XPOS(i-2)])
				   + 2*(ps->q[YPOS(i-1)] - ps->q[YPOS(i-2)]) * (ps->q_dot[YPOS(i-1)] - ps->q_dot[YPOS(i-2)]);
		}
	}
}

void computeMatrix_GradC(struct world *ps, double * GradC) {
	int N = ps->N;
	for (int row = 0; row < N + 2; row++) {
		for (int col = 0; col < 2*N; col++) {
			int index = row * 2*N + col;
			if (row == 0) {
				if (col == 0) {
					GradC[index] = 1.0;
				} else {
					GradC[index] = 0.0;
				}
			} else if(row == 1) {
				if (col == 1) {
					GradC[index] = 1.0;
				} else {
					GradC[index] = 0.0;
				}
			} else if (row == N+1) {
				if (cRingConstraint) {
					if (col == 2*N-2){
						GradC[index] = 2*ps->q[XPOS(row - 2)];
					} else if (col == 2*N-1) {
						GradC[index] = 2*ps->q[YPOS(row - 2)];
					} else {
						GradC[index] = 0.0;
					}
				} else {
					GradC[index] = 0.0;
				}
			} else {
				int k = (row - 2);
				if (col == 2*k) {
					GradC[index] = 2*(ps->q[XPOS(k)] - ps->q[XPOS(k+1)]);
				} else if (col == 2*k + 1) {
					GradC[index] = 2*(ps->q[YPOS(k)] - ps->q[YPOS(k+1)]);
				} else if (col == 2*k + 2) {
					GradC[index] = 2*(ps->q[XPOS(k+1)] - ps->q[XPOS(k)]);
				} else if (col == 2*k + 3) {
					GradC[index] = 2*(ps->q[YPOS(k+1)] - ps->q[YPOS(k)]);
				} else {
					GradC[index] = 0.0;
				}
			}
		}
	}
}

void computeMatrix_GradCdot(struct world *ps, double * GradCdot) {
	int N = ps->N;
	for (int row = 0; row < N + 2; row++) {
		for (int col = 0; col < 2*N; col++) {
			int index = row * 2*N + col;
			if (row == 0) {
				GradCdot[index] = 0.0;
			} else if(row == 1) {
				GradCdot[index] = 0.0;
			} else if (row == N+1) {
				if (cRingConstraint) {
					if (col == 2*N-2){
						GradCdot[index] = 2*ps->q_dot[XPOS(row - 2)];
					} else if (col == 2*N-1) {
						GradCdot[index] = 2*ps->q_dot[YPOS(row - 2)];
					} else {
						GradCdot[index] = 0.0;
					}
				} else {
					GradCdot[index] = 0.0;
				}
			} else {
				int k = (row - 2);
				if (col == 2*k) {
					GradCdot[index] = 2*(ps->q_dot[XPOS(k)] - ps->q_dot[XPOS(k+1)]);
				} else if (col == 2*k + 1) {
					GradCdot[index] = 2*(ps->q_dot[YPOS(k)] - ps->q_dot[YPOS(k+1)]);
				} else if (col == 2*k + 2) {
					GradCdot[index] = 2*(ps->q_dot[XPOS(k+1)] - ps->q_dot[XPOS(k)]);
				} else if (col == 2*k + 3) {
					GradCdot[index] = 2*(ps->q_dot[YPOS(k+1)] - ps->q_dot[YPOS(k)]);
				} else {
					GradCdot[index] = 0.0;
				}
			}
		}
	}
}


void computeMatrix_ConstraintError (struct world *ps, double * matC, double * matGradC, double * matGradCdot, double * E) {
	int N = ps->N;
	double * t1 = (double *)malloc(sizeof(double) * (N+2));
	for (int row = 0; row < (N+2); row++) {
		double sum = 0.0;
		for (int col = 0; col < 2*N; col++) {
			int indexGradCdot = row*(2*N)+col;
			sum = sum + matGradCdot[indexGradCdot] * ps->q_dot[col];
		}
		t1[row] = sum;
	}

	double * t2 = (double *)malloc(sizeof(double) * (N+2));
	for (int row = 0; row < (N+2); row++) {
		double sum = 0.0;
		for (int col = 0; col < 2*N; col++) {
			int indexGradC = row*(2*N)+col;
			sum = sum + matGradC[indexGradC] * ps->q_dot[col];
		}
		t2[row] = sum;
	}

	constraintError1 = 0.0;
	for (int row = 0; row < (N+2); row++) {
		E[row] = -1 * t1[row] - 2 * ps->beta * t2[row] - ps->beta * ps->beta * matC[row];
		constraintError1 += SQ(E[row]);
	}
	constraintError1 = sqrt(constraintError1);
}

void computeMatrix_ConstraintError2 (struct world *ps, double * matC, double * matCdot, double * matGradCdot, double * E) {
	int N = ps->N;
	double * t1 = (double *)malloc(sizeof(double) * (N+2));
	for (int row = 0; row < (N+2); row++) {
		double sum = 0.0;
		for (int col = 0; col < 2*N; col++) {
			int indexGradCdot = row*(2*N)+col;
			sum = sum + matGradCdot[indexGradCdot] * ps->q_dot[col];
		}
		t1[row] = sum;
	}

	for (int row = 0; row < (N+2); row++) {
		E[row] = -1*t1[row] - 2 * ps->beta * matCdot[row] - ps->beta * ps->beta * matC[row];
	}
}

void computeMatrix_M(struct world * ps, double * M) {
	int N = ps->N;
	for (int row = 0; row < 2*N; row++) {
		for (int col = 0; col < 2*N; col++) {
			int index = row*2*N+col;
			if (row == col) {
				M[index] = ps->mass;
			} else {
				M[index] = 0.0;
			}

		}
	}
}

void computeMatrix_matGradCTranspose(struct world * ps, double * matGradC, double * matGradCT) {
	int N = ps->N;
	for (int row = 0; row < N+2; row++) {
		for (int col = 0; col < 2*N; col++) {
			int indexGradC = row*2*N+col;
			int indexGradCT = col*(N+2)+row;
			matGradCT[indexGradCT] = matGradC[indexGradC];
		}
	}
}

void computeMatrix_A(struct world * ps, double * mass, double * matGradC, double * matGradCT, double * A) {
	int N = ps->N;
	for (int row = 0; row < 3*N+2; row++) {
		for (int col = 0; col < 3*N+2; col++) {
			int indexA = row*(3*N+2) + col;
			if (row < 2*N && col < 2*N) {
				int indexMass = row *2*N + col;
				A[indexA] = mass[indexMass];
			} else if (row < 2*N && col >= 2*N) {
				int indexGradCT = row *(N+2) + (col - 2*N);
				A[indexA] = matGradCT[indexGradCT];
			} else if (row >= 2*N && col < 2*N) {
				int indexGradC = (row - 2*N)*2*N + col;
				A[indexA] = matGradC[indexGradC];
			} else {
				A[indexA] = 0.0;
			}
		}
	}
}

void computeMatrix_B(struct world * ps, double * matError, double *B) {
	int N = ps->N;
	for (int row = 0; row < 3*N+2; row++) {
		if (row < 2*N) {
			if (row%2 == 0) {
				B[row] = ps->force[row] - ps->alpha * ps->q_dot[row] + extHoriAcce * ps->mass;
			} else {
				B[row] = ps->force[row] - ps->alpha * ps->q_dot[row] + extVertiAcce * ps->mass;
			}
		} else {
			int indexError = row - 2*N;
			B[row] = matError[indexError];
		}
	}
}

void computeAcceleration(struct world * ps, double * a)
{
	double * matC = (double *) malloc(sizeof(double) * (ps->N + 2));
	computeMatrix_C(ps, matC);
	double * matCdot = (double *) malloc(sizeof(double) * (ps->N + 2));
	computeMatrix_Cdot(ps, matCdot);
	double * matGradC = (double *) malloc(sizeof(double) * ((ps->N + 2) * 2 * ps->N));
	computeMatrix_GradC(ps, matGradC);
	double * matGradCdot = (double *) malloc(sizeof(double) * ((ps->N + 2) * 2 * ps->N));
	computeMatrix_GradCdot(ps, matGradCdot);
	double * matError = (double *) malloc(sizeof(double) * (ps->N + 2));
	computeMatrix_ConstraintError(ps, matC, matGradC, matGradCdot, matError);
	//computeMatrix_ConstraintError2(ps, matC, matCdot, matGradCdot, matError);
	double * mass = (double *) malloc(sizeof(double)*(ps->N)*(ps->N)*4);
	computeMatrix_M(ps, mass);
	double * matGradCTranspose = (double *) malloc(sizeof(double) * ((ps->N + 2) * 2 * ps->N));
	computeMatrix_matGradCTranspose(ps, matGradC, matGradCTranspose);

	// Forming the equations in the form   A x = B
	double * A = (double *) malloc(sizeof(double) * (3*ps->N + 2) * (3*ps->N + 2));
	computeMatrix_A(ps, mass, matGradC, matGradCTranspose, A);

	double * B = (double *) malloc(sizeof(double) * (3*ps->N + 2));
	computeMatrix_B(ps, matError, B);

	gsl_matrix_view m = gsl_matrix_view_array (A, (3*ps->N + 2), (3*ps->N + 2));
	gsl_matrix matA = m.matrix;
	gsl_vector_view b = gsl_vector_view_array (B, (3*ps->N + 2));
	gsl_vector vecB = b.vector;
	gsl_matrix *V = gsl_matrix_alloc((3*ps->N + 2), (3*ps->N + 2));
	gsl_vector *S = gsl_vector_alloc((3*ps->N + 2));
	gsl_vector *work = gsl_vector_alloc((3*ps->N + 2));
	gsl_linalg_SV_decomp(&matA, V, S, work);

	//
	// Output of gsl_linalg_SV_decomp is in the form: A = U S V^T
	// S[0][0] is the max value of the matrix S
	// To handle singularities in the equation
	// Truncate any value in S to zero if that value < max*1e-6;
	//
	double eps = 1e-6;
	double max = gsl_vector_get(S, 0);
	for (int i=0; i<(3*ps->N + 2); i++) {
		if (gsl_vector_get(S, i) < max * eps) {
			gsl_vector_set(S, i, 0.0);
		}
	}

	gsl_vector *X = gsl_vector_alloc ((3*ps->N + 2));
	gsl_linalg_SV_solve(&matA, V, S, &vecB, X);
	for (int i = 0; i < 2 * ps->N; i++) {
			a[i] = X->data[i];
	}

	// Cleanup used memory
	delete [] matC;	delete [] matCdot;
	delete [] matGradC;	delete [] matGradCdot;
	delete [] matError;	delete [] mass;
	delete [] matGradCTranspose;
	delete [] A; delete [] B;

	gsl_matrix_free(V);
	gsl_vector_free(work);
	gsl_vector_free(S);
	gsl_vector_free(X);


}

/* performs one step of Euler Integration */
/* as a result, updates the jello structure */
void Euler(struct world * ps)
{
	double * a = (double *)malloc(sizeof(double) * ps->N * 2);
    computeAcceleration(ps, a);
    for (int i=0; i < 2*ps->N; i++) {
    	ps->q_dot[i] += ps->dt * a[i];
    	ps->q[i] += ps->dt * ps->q_dot[i];
    }
}

//
// performs one step of RK4 Integration
//
void RK4(struct world * ps)
{
	struct world buffer;
    buffer = *ps; // make a copy

	double * a = (double *)malloc(sizeof(double) * ps->N * 2);
    computeAcceleration(ps, a);

	double * k1_p = (double *)malloc(sizeof(double) *  ps->N * 2);
	double * k1_v = (double *)malloc(sizeof(double) *  ps->N * 2);
	for (int i = 0; i < ps->N * 2; i++) {
		k1_p[i] = ps->q_dot[i] * ps->dt;
		k1_v[i] = a[i] * ps->dt;
		buffer.q[i] = ps->q[i] + 0.5*k1_p[i];
		buffer.q_dot[i] = ps->q_dot[i] + 0.5*k1_v[i];
	}
	computeAcceleration(&buffer, a);

	double * k2_p = (double *)malloc(sizeof(double) *  ps->N * 2);
	double * k2_v = (double *)malloc(sizeof(double) *  ps->N * 2);
	for (int i = 0; i < ps->N * 2; i++) {
		k2_p[i] = buffer.q_dot[i] * ps->dt;
		k2_v[i] = a[i] * ps->dt;
		buffer.q[i] = ps->q[i] + 0.5*k2_p[i];
		buffer.q_dot[i] = ps->q_dot[i] + 0.5*k2_v[i];
	}
	computeAcceleration(&buffer, a);

	double * k3_p = (double *)malloc(sizeof(double) *  ps->N * 2);
	double * k3_v = (double *)malloc(sizeof(double) *  ps->N * 2);
	for (int i = 0; i < ps->N * 2; i++) {
		k3_p[i] = buffer.q_dot[i] * ps->dt;
		k3_v[i] = a[i] * ps->dt;
		buffer.q[i] = ps->q[i]  + 0.5 * k3_p[i];
		buffer.q_dot[i] = ps->q_dot[i] + 0.5 * k3_v[i];
	}
	computeAcceleration(&buffer, a);

	double * k4_p = (double *)malloc(sizeof(double) *  ps->N * 2);
	double * k4_v = (double *)malloc(sizeof(double) *  ps->N * 2);
	for (int i = 0; i < ps->N * 2; i++) {
		k4_p[i] = buffer.q_dot[i] * ps->dt;
		k4_v[i] = a[i] * ps->dt;
		ps->q[i] += (k1_p[i] + 2 * k2_p[i] + 2 * k3_p[i] + k4_p[i]) / 6.0;
		ps->q_dot[i] += (k1_v[i] + 2 * k2_v[i] + 2 * k3_v[i] + k4_v[i]) / 6.0;
	}

	delete [] k1_p; delete [] k1_v;
	delete [] k2_p; delete [] k2_v;
	delete [] k3_p; delete [] k3_v;
	delete [] k4_p; delete [] k4_v;
	delete [] a;
}
