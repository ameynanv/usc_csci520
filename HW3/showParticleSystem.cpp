/*

  USC/Viterbi/Computer Science
  "Jello Cube" Assignment 1 starter code

*/

#include "showParticleSystem.h"

#include <iostream>
#include <vector>
#include <string.h>
#include "particleSystem.h"

void drawText(int x, int y, float r, float g, float b, void* font, char * name)
{
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D( 0, 640, 0, 480 );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glColor3f( r, g, b );
	int len = (int)strlen(name);
	glRasterPos2i(x , y);
	for ( int i = 0; i < len; ++i ) {
		glutBitmapCharacter(font, name[i]);
	}
	glPopMatrix();

	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
}

void drawInformationText(struct world *ps) {
	char buffer[200];
	sprintf(buffer, "N = %d, Beta = %.2f, Alpha = %.4f, Method = %s", ps->N, ps->beta, ps->alpha, ps->integrator);
	drawText(10,460,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	sprintf(buffer, "Time = %d s", simulationTime);
	drawText(500,460,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	sprintf(buffer, "A.x = %.2f, A.y = %.2f", extHoriAcce, extVertiAcce + -1.0);
	drawText(10,440,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	if (externalForce) {
		sprintf(buffer, "External Force : ON");
	} else {
		sprintf(buffer, "External Force : OFF");
	}
	drawText(10,420,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	if (timeVaryingField) {
		sprintf(buffer, "Time Varying Field : ON");
	} else {
		sprintf(buffer, "Time Varying Field : OFF");
	}
	drawText(10,400,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);

	sprintf(buffer, "Constraint Error = %.5f", constraintError1);
	drawText(10,380,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);

	sprintf(buffer, "t : Toggle Time Varying Field");
	drawText(10,70,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	sprintf(buffer, "f : Toggle External Force");
	drawText(10,50,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	sprintf(buffer, "a / d : To apply horizontal force in left/right direction");
	drawText(10,30,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
	sprintf(buffer, "w / s : To apply vertical force in up/down direction");
	drawText(10,10,0.0,0.0,0.0,GLUT_BITMAP_HELVETICA_18, buffer);
}

void drawRing(struct world* ps) {
	glRotatef(90, 1.0, 0, 0);
	glutSolidTorus(0.02,ps->ringDiameter/2.0, 100, 100);
}

// Draw all the particles
void drawParticles(struct world* ps) {
	for (int i = 0; i < ps->N; i++) {
		glTranslated(ps->q[2*i], ps->q[2*i+1], 0.0);
		glutSolidSphere(0.3/ps->N, 20, 20);
		glTranslated(-ps->q[2*i], -ps->q[2*i+1], 0.0);
		if (i > 0) {
			glDisable(GL_LIGHTING);
			glLineWidth(6);
			glBegin(GL_LINES);
				// X axis: Red
				glColor4f(0.0, 0.0, 0.0,0.0);
				glVertex3d(ps->q[2 *(i-1)], ps->q[2 * (i-1) + 1], 0.0);
				glVertex3d(ps->q[2 *(i)], ps->q[2 * (i) + 1], 0.0);
			glEnd();
			glEnable(GL_LIGHTING);
		}
	}
}

void drawAxes(float width) {
	glLineWidth(2);
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
		// X axis: Red
		glColor4f(1.0, 0.0, 0.0,0.0);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(width,0.0,0.0);

		// Y axis: Green
		glColor4f(0.0, 1.0, 0.0, 0.0);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,width,0.0);

		// Z axis: Blue
		glColor4f(0.0, 0.0, 1.0,0.0);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,width);

	glEnd();
	glEnable(GL_LIGHTING);
}
