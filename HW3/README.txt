<Please submit this file with your solution.>

CSCI 520, Assignment 3

AMEY VAIDYA
================

<Description of what you have accomplished>
1) Implemented constrained Particle system simulation
2) Implemented manual force field. Use can apply acceleration using keyboard.
3) Implemented Euler and RK4 integrators for simulation.
4) Implemented time varying force field in the particle system.

<How to run the code>
  The program accepts following paramters:
  ./particleSystem [NumberOfParticles] [Euler|RK4] [beta] [alpha]
  NumberOfParticles: This parameters specify how many particles are present in the simulation
  Euler|RK4 : To use Euler integration specify "Euler" or else specify "RK4"
  beta: The value of beta used in the Baumgarte Stabilization
  alpha: The value of damping used to create "underwater effect"
  
  Sample Command:
  ./particleSystem 11 Euler 2.5 0.2

<File structure>
Output/ : Output directory contains all the jpeg images for the simulation
CSCI520HW3.pdf : Detailed description of the equation derivation and answers to questions
Graph.xlsx: Data used for drawing graphs
Graph_Beta0.png, Graph_Beta600: Graphs used in the documentation


<Applying force field manually>
  To enable manual external force field:
  1) Press "f" on the program window. This will enable manual external force field.
  2) To apply a force in X direction use buttons "a" or "d".
  3) "a" applies the force in -ve X direction, whereas "d" applies foce in +ve X direction.
  4) To apply a force in Y direction use buttons "w" or "s".
  5) "w" applies the force in +ve Y direction, whereas "s" applies foce in -ve Y direction.
  6) To stop applying manual force press "f" again.
    
 
<Also, explain any extra credit that you have implemented.>
1) Time Varying Force Field:
       1) Run the program with following arguments: ./particleSystem 11 Euler 2.5 0.2
       2) When the system becomes stable, press "t" to enable time varying field.
       3) The program GUI will indicate if time varying field is enabled or not.
       4) Time varying field will apply a force in +ve X direction for 3 sec.
       5) Then it will allow the system to come to a rest in next 5 sec.
       6) Again the field will apply a force in -ve X direction for next 3 sec.
       7) Then it will allow the system to come to a rest in next 5 sec.
       8) Steps 4-7 will be repeated in loop.
       9) To disable time varying field press "t" again.
       
2) Implemented RK4 integrator:
       1) To use RK4 integrator run the program with command: ./particleSystem 11 RK4 2.5 0.2
