/*

  USC/Viterbi/Computer Science
  "Jello Cube" Assignment 1 starter code

*/


#ifndef _SHOWPARTICLESYSTEM_H_
#define _SHOWPARTICLESYSTEM_H_

// Show Text On Screen
void drawInformationText(struct world* ps);

// Draw the ring
void drawRing(struct world* ps);

// Draw Axis for Reference
// Red: X, Green: Y, Blue: Z
void drawAxes(float width);

// Draw all the particles
void drawParticles(struct world* ps);

#endif
