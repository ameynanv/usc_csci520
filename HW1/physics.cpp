/*

  USC/Viterbi/Computer Science
  "Jello Cube" Assignment 1 starter code

*/

#include "jello.h"
#include "physics.h"
#include <iostream>

using namespace std;

// Given an equation of the plane and position of the mass point,
// This function calculates the collision force if the mass point collides with the 
// the plane. Results are stored in *hook and *damping pointers.
void calculateCollisionForce(double aPlane, double bPlane, double cPlane, double dPlane,
		                     int indexX, int indexY, int indexZ,
							 point *hook, point *damping, struct world * jello)
{
	  double x = jello->p[indexX][indexY][indexZ].x;
	  double y = jello->p[indexX][indexY][indexZ].y;
	  double z = jello->p[indexX][indexY][indexZ].z;
	  double vx = jello->v[indexX][indexY][indexZ].x;
	  double vy = jello->v[indexX][indexY][indexZ].y;
	  double vz = jello->v[indexX][indexY][indexZ].z;
	  point unitNorm, L, Lunit;

	  double initPointValueOnPlane = aPlane * initPos.x + bPlane *initPos.y + cPlane * initPos.z+ dPlane;
	  double currPointValueOnPlane = aPlane * x + bPlane * y + cPlane * z+ dPlane;

	  double intiPointSign = initPointValueOnPlane > 0 ? 1.0 : -1.0;

	  double mag = sqrt(aPlane * aPlane+ bPlane * bPlane + cPlane * cPlane);
	  unitNorm.x = intiPointSign * aPlane / mag;
	  unitNorm.y = intiPointSign * bPlane / mag;
	  unitNorm.z = intiPointSign * cPlane / mag;

	  if (initPointValueOnPlane * currPointValueOnPlane < 0) {
		  // equation of plane is ax + by + cz + d = 0
		  double distance = (aPlane * x + bPlane * y + cPlane * z + dPlane)/ sqrt(aPlane*aPlane+bPlane*bPlane+cPlane*cPlane);
		  L.x = unitNorm.x * distance;
		  L.y = unitNorm.y * distance;
		  L.z = unitNorm.z * distance;
		  /* Lmag is the magnitude of L  */
		  double Lmag = sqrt(L.x * L.x + L.y * L.y + L.z * L.z);
		  /* Lunit is the unit vector in the direction of L  */
		  Lunit.x = L.x/Lmag; Lunit.y = L.y/Lmag; Lunit.z= L.z/Lmag;
		  /* force_hook = -1 * kElastic * (Lmag - restSpringLength) * L  */
		  hook->x += -1 * jello->kCollision * (Lmag)*Lunit.x;
		  hook->y += -1 * jello->kCollision * (Lmag)*Lunit.y;
		  hook->z += -1 * jello->kCollision * (Lmag)*Lunit.z;

		  /* Calculating elastic damping force  */
		  /* tmp is the dot product of velocityDiff and L vector divided by Lmag */
		  double tmp = (vx * L.x + vy * L.y + vz * L.z) / Lmag;
		  /* force_damping = -1 * kDamping * dot(velocityDiff, L)/ Lmag * L  */
		  damping->x += -1 * jello->dCollision* tmp * Lunit.x;
		  damping->y += -1 * jello->dCollision* tmp * Lunit.y;
		  damping->z += -1 * jello->dCollision* tmp * Lunit.z;
	  }
}

// This function performs the trilinear interpolation of the external force field
// Given a point inside the bounding box, this function calculates the external force
// field acting on the point. Results are stored in *extForce pointer.
void calculateExternalForce(int indexX, int indexY, int indexZ, point *extForce, struct world * jello) {

	// We want to find out value of external force at xPos,yPos,zPos;
	double xPos = jello->p[indexX][indexY][indexZ].x;
	double yPos = jello->p[indexX][indexY][indexZ].y;
	double zPos = jello->p[indexX][indexY][indexZ].z;

	if (xPos < -2) xPos = -2;
	if (xPos >  2) xPos =  2;
	if (yPos < -2) yPos = -2;
	if (yPos >  2) yPos =  2;
	if (zPos < -2) zPos = -2;
	if (zPos >  2) zPos =  2;

	// For this point the nearest points on the force grid are at location xLow and xHigh
    // yLow-yHigh and zLow-zHigh
	double xForceIndex = (xPos + 2.00)*(jello->resolution - 1)/4.00;
	int xLow = floor(xForceIndex);
	int xHigh = xLow + 1;
	double yForceIndex = (yPos + 2.00)*(jello->resolution - 1)/4.00;
	int yLow = floor(yForceIndex);
	int yHigh = yLow + 1;
	double zForceIndex = (zPos + 2.00)*(jello->resolution - 1)/4.00;
	int zLow = floor(zForceIndex);
	int zHigh = zLow + 1;

	double fgUnitDist =  4.00 / (jello->resolution - 1);

	// Shifting the coordinate system so that new origin is xLow and xHigh corresponds to unit distance
	double x = xForceIndex - xLow;
	double y = yForceIndex - yLow;
	double z = zForceIndex - zLow;

	if (xHigh > (jello->resolution - 1) || yHigh > (jello->resolution - 1) || zHigh > (jello->resolution - 1)) return;
	point F000 = jello->forceField[xLow  *jello->resolution*jello->resolution + yLow  *jello->resolution + zLow];
	point F001 = jello->forceField[xLow  *jello->resolution*jello->resolution + yLow  *jello->resolution + zHigh];
	point F010 = jello->forceField[xLow  *jello->resolution*jello->resolution + yHigh *jello->resolution + zLow];
	point F011 = jello->forceField[xLow  *jello->resolution*jello->resolution + yHigh *jello->resolution + zHigh];
	point F100 = jello->forceField[xHigh *jello->resolution*jello->resolution + yLow  *jello->resolution + zLow];
	point F101 = jello->forceField[xHigh *jello->resolution*jello->resolution + yLow  *jello->resolution + zHigh];
	point F110 = jello->forceField[xHigh *jello->resolution*jello->resolution + yHigh *jello->resolution + zLow];
	point F111 = jello->forceField[xHigh *jello->resolution*jello->resolution + yHigh *jello->resolution + zHigh];

	extForce->x = F000.x * (1-x) * (1-y) * (1-z) +
			      F001.x * (1-x) * (1-y) * z +
				  F010.x * (1-x) * y * (1-z) +
				  F011.x * (1-x) * y * z +
			      F100.x * x * (1-y) * (1-z) +
				  F101.x * x * (1-y) * z +
				  F110.x * x * y * (1-z) +
				  F111.x * x * y * z;
	extForce->y = F000.y * (1-x) * (1-y) * (1-z) + F001.y * (1-x) * (1-y) * z + F010.y * (1-x)*y*(1-z) + F011.y * (1-x)*y*z +
			      F100.y * x * (1-y) * (1-z) + F101.y * x * (1-y) * z + F110.y * x*y*(1-z) + F111.y * x*y*z;
	extForce->z = F000.z * (1-x) * (1-y) * (1-z) + F001.z * (1-x) * (1-y) * z + F010.z * (1-x)*y*(1-z) + F011.z * (1-x)*y*z +
			      F100.z * x * (1-y) * (1-z) + F101.z * x * (1-y) * z + F110.z * x*y*(1-z) + F111.z * x*y*z;

}

// This function calculates hook force and damping force on a point (indexX, indexY, indexZ)
// exerted by spring between (indexX, indexY, indexZ) and (indexX + disX, indexY + disY, indexZ + disZ)
void calculateElasticForce(int indexX, int indexY, int indexZ, int disX, int disY, int disZ, point *hook, point *damping, struct world * jello) {
	int ip,jp,kp;
	point L;
	point Lunit;
	double Lmag;
	point velocityDiff;
	double tmp;

	ip = indexX + disX;
	jp = indexY + disY;
	kp = indexZ + disZ;

	// Rest length of the spring is equal to its distance from the point
	double rest_spring_length = (1.0/7.0) * sqrt(disX * disX + disY * disY + disZ * disZ);

	// Ignore if the neighbour is located outside of the cube
	if (!((ip>7) || (ip<0) || (jp>7) || (jp<0) || (kp>7) || (kp<0) ))
	{
	   // L = A - B
	   L.x = jello->p[indexX][indexY][indexZ].x - jello->p[ip][jp][kp].x;
	   L.y = jello->p[indexX][indexY][indexZ].y - jello->p[ip][jp][kp].y;
	   L.z = jello->p[indexX][indexY][indexZ].z - jello->p[ip][jp][kp].z;

	   // Lmag is the magnitude of L
	   Lmag = sqrt(L.x * L.x + L.y * L.y + L.z * L.z);

	   // Lunit is the unit vector in the direction of L
	   Lunit.x = L.x/Lmag; Lunit.y = L.y/Lmag; Lunit.z= L.z/Lmag;

	   // force_hook = -1 * kElastic * (Lmag - restSpringLength) * L
	   hook->x += -1 * jello->kElastic * (Lmag - rest_spring_length)*Lunit.x;
	   hook->y += -1 * jello->kElastic * (Lmag - rest_spring_length)*Lunit.y;
	   hook->z += -1 * jello->kElastic * (Lmag - rest_spring_length)*Lunit.z;

	   // Calculating elastic damping force
	   velocityDiff.x = jello->v[indexX][indexY][indexZ].x - jello->v[ip][jp][kp].x;
	   velocityDiff.y = jello->v[indexX][indexY][indexZ].y - jello->v[ip][jp][kp].y;
	   velocityDiff.z = jello->v[indexX][indexY][indexZ].z - jello->v[ip][jp][kp].z;

	   // tmp is the dot product of velocityDiff and L vector divided by Lmag
	   tmp = (velocityDiff.x*L.x + velocityDiff.y * L.y + velocityDiff.z * L.z) / Lmag;

	   // force_damping = -1 * kDamping * dot(velocityDiff, L)/ Lmag * L
	   damping->x += -1 * jello->dElastic * tmp * Lunit.x;
	   damping->y += -1 * jello->dElastic * tmp * Lunit.y;
	   damping->z += -1 * jello->dElastic * tmp * Lunit.z;
	}
}

/* Computes acceleration to every control point of the jello cube,
   which is in state given by 'jello'.
   Returns result in array 'a'. */
void computeAcceleration(struct world * jello, struct point a[8][8][8])
{
  /* for you to implement ... */
  point force_hook[8][8][8];
  point force_damping[8][8][8];
  point collision_hook[8][8][8];
  point collision_damping[8][8][8];
  point external_force[8][8][8];

  int i,j,k,ip,jp,kp;
  double x,y,z;
  point L;
  point Lunit;
  double Lmag;
  point velocityDiff;
  double tmp;
  double rest_spring_length;

  for (i=0; i<=7; i++)
	for (j=0; j<=7; j++)
	  for (k=0; k<=7; k++)
	  {
		  force_hook[i][j][k].x = 0.0; force_hook[i][j][k].y = 0.0; force_hook[i][j][k].z = 0.0;
		  force_damping[i][j][k].x = 0.0; force_damping[i][j][k].y = 0.0; force_damping[i][j][k].z = 0.0;
		  collision_hook[i][j][k].x = 0.0; collision_hook[i][j][k].y = 0.0; collision_hook[i][j][k].z = 0.0;
		  collision_damping[i][j][k].x = 0.0; collision_damping[i][j][k].y = 0.0; collision_damping[i][j][k].z = 0.0;
		  external_force[i][j][k].x = 0.0; external_force[i][j][k].y = 0.0; external_force[i][j][k].z = 0.0;

          // Structural Springs
		  calculateElasticForce(i, j, k, 1, 0, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k,-1, 0, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0, 1, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0,-1, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0, 0, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0, 0,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);

		  // Shear Springs for neighbors at sqrt(2) distance
		  calculateElasticForce(i, j, k,-1,-1, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k,-1, 1, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 1,-1, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 1, 1, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);

		  calculateElasticForce(i, j, k,-1, 0,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k,-1, 0, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 1, 0,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 1, 0, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);

		  calculateElasticForce(i, j, k, 0,-1,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0,-1, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0, 1,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0, 1, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);

		  // Shear Springs for neighbors at sqrt(3) distance
		  calculateElasticForce(i, j, k, 1,-1,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 1,-1, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 1, 1,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 1, 1, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k,-1,-1,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k,-1,-1, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k,-1, 1,-1, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k,-1, 1, 1, &force_hook[i][j][k], &force_damping[i][j][k], jello);

		  // Bend Springs
		  calculateElasticForce(i, j, k, 2, 0, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k,-2, 0, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0, 2, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0,-2, 0, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0, 0, 2, &force_hook[i][j][k], &force_damping[i][j][k], jello);
		  calculateElasticForce(i, j, k, 0, 0,-2, &force_hook[i][j][k], &force_damping[i][j][k], jello);

		  // Calculating Collision Forces
		  calculateCollisionForce(1, 0, 0, 2, i, j, k, &collision_hook[i][j][k], &collision_damping[i][j][k], jello);
		  calculateCollisionForce(-1, 0, 0, 2, i, j, k, &collision_hook[i][j][k], &collision_damping[i][j][k], jello);

		  calculateCollisionForce(0, 1, 0, 2, i, j, k, &collision_hook[i][j][k], &collision_damping[i][j][k], jello);
		  calculateCollisionForce(0, -1, 0, 2, i, j, k, &collision_hook[i][j][k], &collision_damping[i][j][k], jello);

		  calculateCollisionForce(0, 0, 1, 2, i, j, k, &collision_hook[i][j][k], &collision_damping[i][j][k], jello);
		  calculateCollisionForce(0, 0, -1, 2, i, j, k, &collision_hook[i][j][k], &collision_damping[i][j][k], jello);

		  if (jello->incPlanePresent == 1) {
			  calculateCollisionForce(jello->a, jello->b, jello->c, jello->d, i, j, k, &collision_hook[i][j][k], &collision_damping[i][j][k], jello);
		  }

		  if (jello->resolution >= 2) {
		  	  calculateExternalForce(i,j,k, &external_force[i][j][k], jello);
		  }

		  // acceleration = total_force / mass;
		  a[i][j][k].x = (force_hook[i][j][k].x + force_damping[i][j][k].x +
				  collision_hook[i][j][k].x + collision_damping[i][j][k].x +
				  external_force[i][j][k].x + force_manual.x)/ jello->mass;
		  a[i][j][k].y = (force_hook[i][j][k].y + force_damping[i][j][k].y +
				  collision_hook[i][j][k].y + collision_damping[i][j][k].y +
				  external_force[i][j][k].y + force_manual.y)/ jello->mass;
		  a[i][j][k].z = (force_hook[i][j][k].z + force_damping[i][j][k].z +
				  collision_hook[i][j][k].z + collision_damping[i][j][k].z +
				  external_force[i][j][k].z + force_manual.z)/ jello->mass;
	  }
      force_manual.x = 0;
      force_manual.y = 0;
      force_manual.z = 0;
}

/* performs one step of Euler Integration */
/* as a result, updates the jello structure */
void Euler(struct world * jello)
{
  int i,j,k;
  point a[8][8][8];

  computeAcceleration(jello, a);
  
  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
        jello->p[i][j][k].x += jello->dt * jello->v[i][j][k].x;
        jello->p[i][j][k].y += jello->dt * jello->v[i][j][k].y;
        jello->p[i][j][k].z += jello->dt * jello->v[i][j][k].z;
        jello->v[i][j][k].x += jello->dt * a[i][j][k].x;
        jello->v[i][j][k].y += jello->dt * a[i][j][k].y;
        jello->v[i][j][k].z += jello->dt * a[i][j][k].z;

      }
}

/* performs one step of RK4 Integration */
/* as a result, updates the jello structure */
void RK4(struct world * jello)
{
  point F1p[8][8][8], F1v[8][8][8], 
        F2p[8][8][8], F2v[8][8][8],
        F3p[8][8][8], F3v[8][8][8],
        F4p[8][8][8], F4v[8][8][8];

  point a[8][8][8];


  struct world buffer;

  int i,j,k;

  buffer = *jello; // make a copy of jello

  computeAcceleration(jello, a);

  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
         pMULTIPLY(jello->v[i][j][k],jello->dt,F1p[i][j][k]);
         pMULTIPLY(a[i][j][k],jello->dt,F1v[i][j][k]);
         pMULTIPLY(F1p[i][j][k],0.5,buffer.p[i][j][k]);
         pMULTIPLY(F1v[i][j][k],0.5,buffer.v[i][j][k]);
         pSUM(jello->p[i][j][k],buffer.p[i][j][k],buffer.p[i][j][k]);
         pSUM(jello->v[i][j][k],buffer.v[i][j][k],buffer.v[i][j][k]);
      }

  computeAcceleration(&buffer, a);

  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
         // F2p = dt * buffer.v;
         pMULTIPLY(buffer.v[i][j][k],jello->dt,F2p[i][j][k]);
         // F2v = dt * a(buffer.p,buffer.v);     
         pMULTIPLY(a[i][j][k],jello->dt,F2v[i][j][k]);
         pMULTIPLY(F2p[i][j][k],0.5,buffer.p[i][j][k]);
         pMULTIPLY(F2v[i][j][k],0.5,buffer.v[i][j][k]);
         pSUM(jello->p[i][j][k],buffer.p[i][j][k],buffer.p[i][j][k]);
         pSUM(jello->v[i][j][k],buffer.v[i][j][k],buffer.v[i][j][k]);
      }

  computeAcceleration(&buffer, a);

  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
         // F3p = dt * buffer.v;
         pMULTIPLY(buffer.v[i][j][k],jello->dt,F3p[i][j][k]);
         // F3v = dt * a(buffer.p,buffer.v);     
         pMULTIPLY(a[i][j][k],jello->dt,F3v[i][j][k]);
         pMULTIPLY(F3p[i][j][k],0.5,buffer.p[i][j][k]);
         pMULTIPLY(F3v[i][j][k],0.5,buffer.v[i][j][k]);
         pSUM(jello->p[i][j][k],buffer.p[i][j][k],buffer.p[i][j][k]);
         pSUM(jello->v[i][j][k],buffer.v[i][j][k],buffer.v[i][j][k]);
      }
         
  computeAcceleration(&buffer, a);


  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
         // F3p = dt * buffer.v;
         pMULTIPLY(buffer.v[i][j][k],jello->dt,F4p[i][j][k]);
         // F3v = dt * a(buffer.p,buffer.v);     
         pMULTIPLY(a[i][j][k],jello->dt,F4v[i][j][k]);

         pMULTIPLY(F2p[i][j][k],2,buffer.p[i][j][k]);
         pMULTIPLY(F3p[i][j][k],2,buffer.v[i][j][k]);
         pSUM(buffer.p[i][j][k],buffer.v[i][j][k],buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],F1p[i][j][k],buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],F4p[i][j][k],buffer.p[i][j][k]);
         pMULTIPLY(buffer.p[i][j][k],1.0 / 6,buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],jello->p[i][j][k],jello->p[i][j][k]);

         pMULTIPLY(F2v[i][j][k],2,buffer.p[i][j][k]);
         pMULTIPLY(F3v[i][j][k],2,buffer.v[i][j][k]);
         pSUM(buffer.p[i][j][k],buffer.v[i][j][k],buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],F1v[i][j][k],buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],F4v[i][j][k],buffer.p[i][j][k]);
         pMULTIPLY(buffer.p[i][j][k],1.0 / 6,buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],jello->v[i][j][k],jello->v[i][j][k]);
      }

  return;  
}
