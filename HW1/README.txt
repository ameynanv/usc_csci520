<Please submit this file with your solution.>

CSCI 520, Assignment 1

AMEY VAIDYA
================

<Description of what you have accomplished>
1) Implemented Structural, Shear and Bend springs in 8x8x8 mass point based jello cube structure.
2) Implemented collision detection algorithm in a generic way. 
3) Implemented Trilinear interpolation of the external force field on the cube mass points
4) Implemented collision detection on arbitrary plane positioned inside the bounding box.
5) User can use mouse Left click to apply manual force in XY plane.
 
<Also, explain any extra credit that you have implemented.>
1) Inclined Plane Response:
   Algorithm for evaluating the collision force:
       1) Equation of the plane is defined as Ax+By+Cz+D=0. If a point (x,y,z) lies on the plane, then the value of the Ax+By+Cz+D is equal to 0.
       2) If the point is not on the plane, then Ax+By+Cz+D < 0 or Ax+By+Cz+D > 0. 
       3) To determine, when a point goes out of the bounding plane we assume that for initial condition (time=0) the cube remains completely inside the bounding box.
       4) Now for initial condition we evaluate Ax+By+Cz+D, and determine the sign. 
       5) If for initial condition, Ax+By+Cz+D > 0 then for a given point x1,y1,z1 : A*x1 + B*y1 + C*z1 + D < 0 means that x1,y1,z1 is out of the bounding box.
       6) If for initial condition, Ax+By+Cz+D < 0 then for a given point x1,y1,z1 : A*x1 + B*y1 + C*z1 + D > 0 means that x1,y1,z1 is out of the bounding box.
       7) So at every instant I check if the any point of the cube is out of the bounding box. If it is out of the bounding box, then I apply collision force.
       8) Collision force is proportional to the normal distance between the plane and the point. And the direction of the collision force is normal to the plane.
  Algorithm for rendering the plane:
       1) First find intersection of the plane Ax+By+Cz+D=0 with the 12 edges of the bounding box. If the intersection point is inside the bounding box only then it is valid.
       2) After finding all the intersection points, find the centroid of the polygon formed by connecting the intersection points.
       3) Use OpenGl TRIANGLE_FAN to render the plane starting from centroid point and iterating across all the vertices.
         
2) Applying external force manually using mouse:
       1) Manual force can be applied by using LEFT mouse drag and click.
       2) The magnitude and the direction of the force applied is visualized using a RED arrow in the center of the bounding box.
       3) For this user needs to press LEFT MOUSE button and drag the mouse in certain direction as specified:
       4) If we drag mouse from LEFT to RIGHT, it applies a force along positive X direction.
       5) If we drag mouse from RIGHT to LEFT, it applies a force along negative X direction.
       6) If we drag mouse from BOTTOM to TOP, it applies a force along positive Y direction.
       7) If we drag mouse from TOP to BOTTOM, it applies a force along negative Y direction. 
