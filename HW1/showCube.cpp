/*

  USC/Viterbi/Computer Science
  "Jello Cube" Assignment 1 starter code

*/

#include "jello.h"
#include "showCube.h"
#include <iostream>
#include <vector>

int pointMap(int side, int i, int j)
{
  int r;

  switch (side)
  {
  case 1: //[i][j][0] bottom face
    r = 64 * i + 8 * j;
    break;
  case 6: //[i][j][7] top face
    r = 64 * i + 8 * j + 7;
    break;
  case 2: //[i][0][j] front face
    r = 64 * i + j;
    break;
  case 5: //[i][7][j] back face
    r = 64 * i + 56 + j;
    break;
  case 3: //[0][i][j] left face
    r = 8 * i + j;
    break;
  case 4: //[7][i][j] right face
    r = 448 + 8 * i + j;
    break;
  }

  return r;
}

void showCube(struct world * jello)
{
  int i,j,k,ip,jp,kp;
  point r1,r2,r3; // aux variables
  
  /* normals buffer and counter for Gourad shading*/
  struct point normal[8][8];
  int counter[8][8];

  int face;
  double faceFactor, length;

  if (fabs(jello->p[0][0][0].x) > 10)
  {
    printf ("Your cube somehow escaped way out of the box.\n");
    exit(0);
  }

  
  #define NODE(face,i,j) (*((struct point * )(jello->p) + pointMap((face),(i),(j))))

  
  #define PROCESS_NEIGHBOUR(di,dj,dk) \
    ip=i+(di);\
    jp=j+(dj);\
    kp=k+(dk);\
    if\
    (!( (ip>7) || (ip<0) ||\
      (jp>7) || (jp<0) ||\
    (kp>7) || (kp<0) ) && ((i==0) || (i==7) || (j==0) || (j==7) || (k==0) || (k==7))\
       && ((ip==0) || (ip==7) || (jp==0) || (jp==7) || (kp==0) || (kp==7))) \
    {\
      glVertex3f(jello->p[i][j][k].x,jello->p[i][j][k].y,jello->p[i][j][k].z);\
      glVertex3f(jello->p[ip][jp][kp].x,jello->p[ip][jp][kp].y,jello->p[ip][jp][kp].z);\
    }\

 
  if (viewingMode==0) // render wireframe
  {
    glLineWidth(1);
    glPointSize(5);
    glDisable(GL_LIGHTING);
    for (i=0; i<=7; i++)
      for (j=0; j<=7; j++)
        for (k=0; k<=7; k++)
        {
          if (i*j*k*(7-i)*(7-j)*(7-k) != 0) // not surface point
            continue;

          glBegin(GL_POINTS); // draw point
            glColor4f(0,0,0,0);  
            glVertex3f(jello->p[i][j][k].x,jello->p[i][j][k].y,jello->p[i][j][k].z);        
          glEnd();

          //
          //if ((i!=7) || (j!=7) || (k!=7))
          //  continue;

          glBegin(GL_LINES);      
          // structural
          if (structural == 1)
          {
            glColor4f(0,0,1,1);
            PROCESS_NEIGHBOUR(1,0,0);
            PROCESS_NEIGHBOUR(0,1,0);
            PROCESS_NEIGHBOUR(0,0,1);
            PROCESS_NEIGHBOUR(-1,0,0);
            PROCESS_NEIGHBOUR(0,-1,0);
            PROCESS_NEIGHBOUR(0,0,-1);
          }
          
          // shear
          if (shear == 1)
          {
            glColor4f(0,1,0,1);
            PROCESS_NEIGHBOUR(1,1,0);
            PROCESS_NEIGHBOUR(-1,1,0);
            PROCESS_NEIGHBOUR(-1,-1,0);
            PROCESS_NEIGHBOUR(1,-1,0);
            PROCESS_NEIGHBOUR(0,1,1);
            PROCESS_NEIGHBOUR(0,-1,1);
            PROCESS_NEIGHBOUR(0,-1,-1);
            PROCESS_NEIGHBOUR(0,1,-1);
            PROCESS_NEIGHBOUR(1,0,1);
            PROCESS_NEIGHBOUR(-1,0,1);
            PROCESS_NEIGHBOUR(-1,0,-1);
            PROCESS_NEIGHBOUR(1,0,-1);

            PROCESS_NEIGHBOUR(1,1,1)
            PROCESS_NEIGHBOUR(-1,1,1)
            PROCESS_NEIGHBOUR(-1,-1,1)
            PROCESS_NEIGHBOUR(1,-1,1)
            PROCESS_NEIGHBOUR(1,1,-1)
            PROCESS_NEIGHBOUR(-1,1,-1)
            PROCESS_NEIGHBOUR(-1,-1,-1)
            PROCESS_NEIGHBOUR(1,-1,-1)
          }
          
          // bend
          if (bend == 1)
          {
            glColor4f(1,0,0,1);
            PROCESS_NEIGHBOUR(2,0,0);
            PROCESS_NEIGHBOUR(0,2,0);
            PROCESS_NEIGHBOUR(0,0,2);
            PROCESS_NEIGHBOUR(-2,0,0);
            PROCESS_NEIGHBOUR(0,-2,0);
            PROCESS_NEIGHBOUR(0,0,-2);
          }           
          glEnd();
        }
    glEnable(GL_LIGHTING);
  }
  
  else
  {
    glPolygonMode(GL_FRONT, GL_FILL); 
    
    for (face=1; face <= 6; face++) 
      // face == face of a cube
      // 1 = bottom, 2 = front, 3 = left, 4 = right, 5 = far, 6 = top
    {
      
      if ((face==1) || (face==3) || (face==5))
        faceFactor=-1; // flip orientation
      else
        faceFactor=1;
      

      for (i=0; i <= 7; i++) // reset buffers
        for (j=0; j <= 7; j++)
        {
          normal[i][j].x=0;normal[i][j].y=0;normal[i][j].z=0;
          counter[i][j]=0;
        }

      /* process triangles, accumulate normals for Gourad shading */
  
      for (i=0; i <= 6; i++)
        for (j=0; j <= 6; j++) // process block (i,j)
        {
          pDIFFERENCE(NODE(face,i+1,j),NODE(face,i,j),r1); // first triangle
          pDIFFERENCE(NODE(face,i,j+1),NODE(face,i,j),r2);
          CROSSPRODUCTp(r1,r2,r3); pMULTIPLY(r3,faceFactor,r3);
          pNORMALIZE(r3);
          pSUM(normal[i+1][j],r3,normal[i+1][j]);
          counter[i+1][j]++;
          pSUM(normal[i][j+1],r3,normal[i][j+1]);
          counter[i][j+1]++;
          pSUM(normal[i][j],r3,normal[i][j]);
          counter[i][j]++;

          pDIFFERENCE(NODE(face,i,j+1),NODE(face,i+1,j+1),r1); // second triangle
          pDIFFERENCE(NODE(face,i+1,j),NODE(face,i+1,j+1),r2);
          CROSSPRODUCTp(r1,r2,r3); pMULTIPLY(r3,faceFactor,r3);
          pNORMALIZE(r3);
          pSUM(normal[i+1][j],r3,normal[i+1][j]);
          counter[i+1][j]++;
          pSUM(normal[i][j+1],r3,normal[i][j+1]);
          counter[i][j+1]++;
          pSUM(normal[i+1][j+1],r3,normal[i+1][j+1]);
          counter[i+1][j+1]++;
        }

      
        /* the actual rendering */
        for (j=1; j<=7; j++) 
        {

          if (faceFactor  > 0)
            glFrontFace(GL_CCW); // the usual definition of front face
          else
            glFrontFace(GL_CW); // flip definition of orientation
         
          glBegin(GL_TRIANGLE_STRIP);
          for (i=0; i<=7; i++)
          {
            glNormal3f(normal[i][j].x / counter[i][j],normal[i][j].y / counter[i][j],
              normal[i][j].z / counter[i][j]);
            glVertex3f(NODE(face,i,j).x, NODE(face,i,j).y, NODE(face,i,j).z);
            glNormal3f(normal[i][j-1].x / counter[i][j-1],normal[i][j-1].y/ counter[i][j-1],
              normal[i][j-1].z / counter[i][j-1]);
            glVertex3f(NODE(face,i,j-1).x, NODE(face,i,j-1).y, NODE(face,i,j-1).z);
          }
          glEnd();
        }
        
        
    }  
  } // end for loop over faces
  glFrontFace(GL_CCW);
}

void showBoundingBox()
{
  int i,j;

  glColor4f(0.6,0.6,0.6,0);

  glBegin(GL_LINES);

  // front face
  for(i=-2; i<=2; i++)
  {
    glVertex3f(i,-2,-2);
    glVertex3f(i,-2,2);
  }
  for(j=-2; j<=2; j++)
  {
    glVertex3f(-2,-2,j);
    glVertex3f(2,-2,j);
  }

  // back face
  for(i=-2; i<=2; i++)
  {
    glVertex3f(i,2,-2);
    glVertex3f(i,2,2);
  }
  for(j=-2; j<=2; j++)
  {
    glVertex3f(-2,2,j);
    glVertex3f(2,2,j);
  }

  // left face
  for(i=-2; i<=2; i++)
  {
    glVertex3f(-2,i,-2);
    glVertex3f(-2,i,2);
  }
  for(j=-2; j<=2; j++)
  {
    glVertex3f(-2,-2,j);
    glVertex3f(-2,2,j);
  }

  // right face
  for(i=-2; i<=2; i++)
  {
    glVertex3f(2,i,-2);
    glVertex3f(2,i,2);
  }
  for(j=-2; j<=2; j++)
  {
    glVertex3f(2,-2,j);
    glVertex3f(2,2,j);
  }
  
  glEnd();

  return;
}

// This method finds the intersection point of a plane Ax + By + Cz + D = 0
// with the bounding box edges.
// If intersection point contains within the bounding box, only then it is added
// to the vector.
void findIntersectionPoint(double A, double B, double C, double D,
		                   double x, double y, double z,
						   std::vector<point> *points) {
	if (x == 0.0) {
		x = -1 * (B*y + C*z + D)/A;
	} else if (y == 0.0) {
		y = -1 * (A*x + C*z + D)/B;
	} else if (z == 0.0) {
		z = -1 * (A*x + B*y + D)/C;
	}
	if (x <= 2 && x >= -2 && y <= 2 && y >= -2 && z <= 2 && z >= -2) {
		point p;
		p.x = x;
		p.y = y;
		p.z = z;
		points->push_back(p);
	}
}

// Given the vertices of a polygon, this method finds the
// centroid of the polygon. This is used to draw the inclined plane
void findCentroid(point* centroid, std::vector<point> *points) {
	centroid->x = 0; centroid->y = 0; centroid->z = 0;
	std::vector<point>::const_iterator i;
	for(i=points->begin(); i != points->end(); i++){
		centroid->x += (*i).x; centroid->y += (*i).y; centroid->z += (*i).z;
	}
	centroid->x /= points->size();
	centroid->y /= points->size();
	centroid->z /= points->size();
}

// This method draws the inclined plane
void showInclinedPlane(struct world * jello) {
	glPointSize(1);
	glColor4f(1,1,0.6,1); // X axis Red
    std::vector<point> intersectionPoints;

	double A = jello->a;
	double B = jello->b;
	double C = jello->c;
	double D = jello->d;

	// Equation of plane is Ax + By + Cz + D = 0
	// The cube has 12 Edges
	findIntersectionPoint(A, B, C, D, -2, -2,  0, &intersectionPoints);
	findIntersectionPoint(A, B, C, D, -2,  2,  0, &intersectionPoints);
	findIntersectionPoint(A, B, C, D,  2, -2,  0, &intersectionPoints);
	findIntersectionPoint(A, B, C, D,  2,  2,  0, &intersectionPoints);

	findIntersectionPoint(A, B, C, D,  0, -2, -2, &intersectionPoints);
	findIntersectionPoint(A, B, C, D,  0, -2,  2, &intersectionPoints);
	findIntersectionPoint(A, B, C, D,  0,  2, -2, &intersectionPoints);
	findIntersectionPoint(A, B, C, D,  0,  2,  2, &intersectionPoints);

	findIntersectionPoint(A, B, C, D, -2,  0, -2, &intersectionPoints);
	findIntersectionPoint(A, B, C, D, -2,  0,  2, &intersectionPoints);
	findIntersectionPoint(A, B, C, D,  2,  0, -2, &intersectionPoints);
	findIntersectionPoint(A, B, C, D,  2,  0,  2, &intersectionPoints);

	point centroid;
	findCentroid(&centroid, &intersectionPoints);
	double mag = sqrt(A*A+B*B+C*C);
	glBegin(GL_TRIANGLE_FAN);
		glVertex3f(centroid.x,centroid.y,centroid.z);
		std::vector<point>::const_iterator i;
		for(i=intersectionPoints.begin(); i != intersectionPoints.end(); i++){
			glVertex3f((*i).x, (*i).y, (*i).z);
            glNormal3f(A/mag,B/mag,C/mag);
		}
		i=intersectionPoints.begin();
		glVertex3f((*i).x, (*i).y, (*i).z);
	glEnd();
}


void showManualForceDirection() {
	if (abs(mouseCurrentForce.x) > 0 || abs(mouseCurrentForce.y) > 0) {
		glLineWidth(5);
		glDisable(GL_LIGHTING);
		glColor4f(1,0,0,1);
		glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(mouseCurrentForce.x/320.00,mouseCurrentForce.y/240.00,0);
		glEnd();
		glPushMatrix();
		glTranslated(mouseCurrentForce.x/320.00,mouseCurrentForce.y/240.00,0);
		glRotated(90.0,-1*mouseCurrentForce.y/240.00,mouseCurrentForce.x/320.00,0);
		glutSolidCone(0.1, 0.2, 10, 10);
		glPopMatrix();
		glEnable(GL_LIGHTING);
		glLineWidth(1);
	}
}
