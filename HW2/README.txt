Name: AMEY VAIDYA
USCID: 2836139499
==================
1) Development Environment: 
    Ubuntu 64bit

2) Folder Structure:
    HW2/fltk-1.3.2: Code to Fltk library.
    HW2/IDE-starter: IDE for windows system
    HW2/mocapPlayer-starter: Mocap Player code, Interpolation code, Code to extract angles from AMC files
    HW2/Output: Contains the output videos and graphs
    HW2/README.txt: README file
    HW2/Report.pdf: Homework report containing observations, comparisons and extra credit details

3) Instructions to compile the code:
    a) Go to HW2/fltk-1.3.2 folder. Run "make "
    b) Go to HW2/mocapPlayer-starter folder. Run "make "
    c) To run interpolation, use command "./interpolate <skeletonfile> <motionfile> <l|b> <e|q> N out.amc"
    d) To run mocapPlayer, use command "./mocapPlayer "
    e) To extract data from drawing graph into a csv file, use command "./extractFromAMC <input amc file> <output csv file>"

4) Extra Credit Implemented:
    a) I have added a button on the mocapPlayer to enable texture on the ground plane. To see the texture run mocap player. 
        The texture button is located on the right panel, below Ground button. The texture is generated using Perlin noise.
    b) I have added performance counter to the interpolate method. This is used to measure the time required for interpolation.
        whenever interpolation is run using the command "./interpolate <skeletonfile> <motionfile> <l|b> <e|q> N out.amc", the 
        time required for interpolation is printed on the console. The time is in seconds.

5) Detailed extra credit explaination and interpolation analysis is present in the report Report.pdf

6) Output Videos and Graphs:
    a) Output folder contains folders V1, V2, V3 each containing the required videos.
       HW2/Output/V1/BE_martialArts.mp4 : Input motion and Bazier Euler interpolation for 135_06-martialArts.amc, N=40
       HW2/Output/V2/LQ_martialArts.mp4 : Input motion and SLERP Quaternion interpolation for 135_06-martialArts.amc, N=40
       HW2/Output/V3/BQ_martialArts.mp4 : Input motion and Bazier Quaternion interpolation for 135_06-martialArts.amc, N=40
    b) Ouput folder contains following graphs:
       HW2/Output/graph1.png : lfemur X rotation, Linear Euler vs Bazier Euler vs Input
       HW2/Output/graph2.png : lfemur X rotation, Linear Quaternion vs Bazier Quaternion vs Input
       HW2/Output/graph3.png : root Z rotation, Linear Euler vs Linear Quaternion vs Input
       HW3/Output/graph4.png : root Z rotation, Bazier Euler vs Bazier Quaternion vs Input

