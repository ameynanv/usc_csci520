#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include "motion.h"
#include "interpolator.h"
#include "types.h"
#include "performanceCounter.h"


Interpolator::Interpolator()
{
  //Set default interpolation type
  m_InterpolationType = LINEAR;

  //set default angle representation to use for interpolation
  m_AngleRepresentation = EULER;
}

Interpolator::~Interpolator()
{
}

//Create interpolated motion
void Interpolator::Interpolate(Motion * pInputMotion, Motion ** pOutputMotion, int N) 
{
  PerformanceCounter counter;
  //Allocate new motion
  *pOutputMotion = new Motion(pInputMotion->GetNumFrames(), pInputMotion->GetSkeleton()); 
  counter.StartCounter();
  //Perform the interpolation
  if ((m_InterpolationType == LINEAR) && (m_AngleRepresentation == EULER))
    LinearInterpolationEuler(pInputMotion, *pOutputMotion, N);
  else if ((m_InterpolationType == LINEAR) && (m_AngleRepresentation == QUATERNION))
    LinearInterpolationQuaternion(pInputMotion, *pOutputMotion, N);
  else if ((m_InterpolationType == BEZIER) && (m_AngleRepresentation == EULER))
    BezierInterpolationEuler(pInputMotion, *pOutputMotion, N);
  else if ((m_InterpolationType == BEZIER) && (m_AngleRepresentation == QUATERNION))
    BezierInterpolationQuaternion(pInputMotion, *pOutputMotion, N);
  else
  {
	counter.StopCounter();
    printf("Error: unknown interpolation / angle representation type.\n");
    exit(1);
  }
  counter.StopCounter();
  double timeForInterpolation = counter.GetElapsedTime();
  printf("Time for interpolation : %.6f sec \n", timeForInterpolation);
}

void Interpolator::LinearInterpolationEuler(Motion * pInputMotion, Motion * pOutputMotion, int N)
{
  int inputLength = pInputMotion->GetNumFrames(); // frames are indexed 0, ..., inputLength-1

  int startKeyframe = 0;
  while (startKeyframe + N + 1 < inputLength)
  {
    int endKeyframe = startKeyframe + N + 1;

    Posture * startPosture = pInputMotion->GetPosture(startKeyframe);
    Posture * endPosture = pInputMotion->GetPosture(endKeyframe);

    // copy start and end keyframe
    pOutputMotion->SetPosture(startKeyframe, *startPosture);
    pOutputMotion->SetPosture(endKeyframe, *endPosture);

    // interpolate in between
    for(int frame=1; frame<=N; frame++)
    {
      Posture interpolatedPosture;
      double t = 1.0 * frame / (N+1);

      // interpolate root position
      interpolatedPosture.root_pos = startPosture->root_pos * (1-t) + endPosture->root_pos * t;

      // interpolate bone rotations
      for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++)
        interpolatedPosture.bone_rotation[bone] = startPosture->bone_rotation[bone] * (1-t) + endPosture->bone_rotation[bone] * t;

      pOutputMotion->SetPosture(startKeyframe + frame, interpolatedPosture);
    }

    startKeyframe = endKeyframe;
  }

  for(int frame=startKeyframe+1; frame<inputLength; frame++)
    pOutputMotion->SetPosture(frame, *(pInputMotion->GetPosture(frame)));
}

void Interpolator::Rotation2Euler(double R[9], double angles[3])
{
  double cy = sqrt(R[0]*R[0] + R[3]*R[3]);

  if (cy > 16*DBL_EPSILON) 
  {
    angles[0] = atan2(R[7], R[8]);
    angles[1] = atan2(-R[6], cy);
    angles[2] = atan2(R[3], R[0]);
  } 
  else 
  {
    angles[0] = atan2(-R[5], R[4]);
    angles[1] = atan2(-R[6], cy);
    angles[2] = 0;
  }

  for(int i=0; i<3; i++)
    angles[i] *= 180 / M_PI;
}

// This function multiplies 3x3 martix A with 3x3 matrix B and stores result in 
// 3x3 Result matrix
void MatMul(double *A, double *B, double *Result) {
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			double sum = 0;
			for (int k = 0; k < 3; k++) {
				sum += A[i*3+k] * B[k*3+j];
			}
			Result[i*3+j] = sum;
		}
	}
}

void Interpolator::Euler2Rotation(double angles[3], double R[9])
{
  // students should implement this
	double matX[9]= {	1, 						0, 						0,
						0, 						cos(RAD(angles[0])), 	-sin(RAD(angles[0])),
						0, 						sin(RAD(angles[0])), 	cos(RAD(angles[0]))
					};
	double matY[9]= {	cos(RAD(angles[1])), 	0, 						sin(RAD(angles[1])),
						0, 						1, 						0,
						-sin(RAD(angles[1])),	0, 						cos(RAD(angles[1]))
					};
	double matZ[9]= {	cos(RAD(angles[2])), 	-sin(RAD(angles[2])), 	0,
						sin(RAD(angles[2])), 	cos(RAD(angles[2])),	0,
						0,						0,						1
					};
	double t[9];
	MatMul(matY, matX, t);
	MatMul(matZ, t, R);
}

// This method performs linear interpolation between vectors pStart and pEnd
vector Interpolator::Lerp(double t, vector & pStart, vector & pEnd_)
{
	vector result =  pStart*(1 - t) + pEnd_*t;
	return result;
}

// This function is used to find the first control point for Bazier Curve calculation
vector Interpolator::FindControlPoint1(vector & pPrev, vector & pStart, vector &pEnd)
{
	vector result;
	vector temp = Lerp(2.0, pPrev, pStart);
	vector an_bar = Lerp(0.5, temp, pEnd);
	result = Lerp(1.0/3.0, pStart, an_bar);
	return result;
}

// This function is used to find the second control point for Bazier Curve calculation
vector Interpolator::FindControlPoint2(vector & pPrev, vector & pStart, vector &pEnd)
{
	vector result;
	vector temp = Lerp(2.0, pPrev, pStart);
	vector an_bar = Lerp(0.5, temp, pEnd);
	result = Lerp(-1.0/3.0, pStart, an_bar);
	return result;
}

// This function is used to find the first control point for Bazier Curve calculation for Quaternions
Quaternion<double> Interpolator::FindControlPoint1(Quaternion<double> & pPrev, Quaternion<double> & pStart, Quaternion<double> &pEnd)
{
	Quaternion<double> result;
	Quaternion<double> temp = Slerp(2.0, pPrev, pStart);
	Quaternion<double> an_bar = Slerp(0.5, temp, pEnd);
	result = Slerp(1.0/3.0, pStart, an_bar);
	return result;
}

// This function is used to find the second control point for Bazier Curve calculation for Quaternions
Quaternion<double> Interpolator::FindControlPoint2(Quaternion<double> & pPrev, Quaternion<double> & pStart, Quaternion<double> &pEnd)
{
	Quaternion<double> result;
	Quaternion<double> temp = Slerp(2.0, pPrev, pStart);
	Quaternion<double> an_bar = Slerp(0.5, temp, pEnd);
	result = Slerp(-1.0/3.0, pStart, an_bar);
	return result;
}

// Given the start key frame number, this function finds the two control points for Bazier interpolation
void Interpolator::FindControlPointsForBezierEulerInterpolation(int startKeyframe, int N, Motion * pInputMotion, vector& A_root_pos, vector& B_root_pos, vector* A_bone, vector* B_bone)
{
	int inputLength = pInputMotion->GetNumFrames();
	int endKeyframe = startKeyframe + N + 1;
	int prevKeyframe = startKeyframe - N - 1;
	int nextKeyframe = endKeyframe + N + 1;
	Posture * startPosture = pInputMotion->GetPosture(startKeyframe);
	Posture * endPosture = pInputMotion->GetPosture(endKeyframe);

	if (startKeyframe == 0) {
		// For first key frame, derive A1 = lerp(p1, lerp(p3, p2, 2.0), 1.0 / 3);

		Posture * nextPosture = pInputMotion->GetPosture(nextKeyframe);
		vector temp = Lerp(2.0, nextPosture->root_pos, endPosture->root_pos);
		A_root_pos = Lerp(1.0/3.0, startPosture->root_pos, temp);
		B_root_pos = FindControlPoint2(startPosture->root_pos, endPosture->root_pos, nextPosture->root_pos);
		for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
			vector temp = Lerp(2.0, nextPosture->bone_rotation[bone], endPosture->bone_rotation[bone]);
			A_bone[bone] = Lerp(1.0/3.0, startPosture->bone_rotation[bone], temp);
			B_bone[bone] = FindControlPoint2(startPosture->bone_rotation[bone], endPosture->bone_rotation[bone], nextPosture->bone_rotation[bone]);
		}
	} else if (nextKeyframe > inputLength) {
		// For last key frame, derive Bn = lerp(pN, Slerp(pN-2, pN-1, 2.0), 1.0 / 3)

		Posture * prevPosture = pInputMotion->GetPosture(prevKeyframe);
		A_root_pos = FindControlPoint1(prevPosture->root_pos, startPosture->root_pos, endPosture->root_pos);
		vector temp = Lerp(2.0, prevPosture->root_pos, startPosture->root_pos);
		B_root_pos = Lerp(1.0/3.0, endPosture->root_pos, temp);
		for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
			A_bone[bone] = FindControlPoint1(prevPosture->bone_rotation[bone], startPosture->bone_rotation[bone], endPosture->bone_rotation[bone]);
			vector temp = Lerp(2.0, prevPosture->bone_rotation[bone], startPosture->bone_rotation[bone]);
			B_bone[bone] = Lerp(1.0/3.0, endPosture->bone_rotation[bone], temp);
		}
	} else {
		Posture * prevPosture = pInputMotion->GetPosture(prevKeyframe);
		Posture * nextPosture = pInputMotion->GetPosture(nextKeyframe);
		A_root_pos = FindControlPoint1(prevPosture->root_pos, startPosture->root_pos, endPosture->root_pos);
		B_root_pos = FindControlPoint2(startPosture->root_pos, endPosture->root_pos, nextPosture->root_pos);
		for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
			A_bone[bone] = FindControlPoint1(prevPosture->bone_rotation[bone], startPosture->bone_rotation[bone], endPosture->bone_rotation[bone]);
			B_bone[bone] = FindControlPoint2(startPosture->bone_rotation[bone], endPosture->bone_rotation[bone], nextPosture->bone_rotation[bone]);
		}
	}
}

// Given the start key frame number, this function finds the two control points for Bazier interpolation using quaternions
void Interpolator::FindControlPointsForBezierQuaternionInterpolation(int startKeyframe, int N, Motion * pInputMotion, vector& A_root_pos, vector& B_root_pos, Quaternion<double>* A_bone, Quaternion<double>* B_bone)
{
	int inputLength = pInputMotion->GetNumFrames();
	int endKeyframe = startKeyframe + N + 1;
	int prevKeyframe = startKeyframe - N - 1;
	int nextKeyframe = endKeyframe + N + 1;
	Posture * startPosture = pInputMotion->GetPosture(startKeyframe);
	Posture * endPosture = pInputMotion->GetPosture(endKeyframe);
	if (startKeyframe == 0) {
		// For first key frame, derive A1 = Slerp(q1, lerp(q3, q2, 2.0), 1.0 / 3);

		Posture * nextPosture = pInputMotion->GetPosture(nextKeyframe);
		vector temp = Lerp(2.0, nextPosture->root_pos, endPosture->root_pos);
		A_root_pos = Lerp(1.0/3.0, startPosture->root_pos, temp);
		B_root_pos = FindControlPoint2(startPosture->root_pos, endPosture->root_pos, nextPosture->root_pos);
		for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
			Quaternion<double> startAngle; Euler2Quaternion(startPosture->bone_rotation[bone].p, startAngle);
			Quaternion<double> endAngle; Euler2Quaternion(endPosture->bone_rotation[bone].p, endAngle);
			Quaternion<double> nextAngle; Euler2Quaternion(nextPosture->bone_rotation[bone].p, nextAngle);

			Quaternion<double> temp = Slerp(2.0, nextAngle, endAngle);
			A_bone[bone] = Slerp(1.0/3.0, startAngle, temp);
			B_bone[bone] = FindControlPoint2(startAngle, endAngle, nextAngle);
		}
	} else if (nextKeyframe > inputLength) {
		// For last key frame, derive Bn = Slerp(qN, Slerp(qN-2, qN-1, 2.0), 1.0 / 3)

		Posture * prevPosture = pInputMotion->GetPosture(prevKeyframe);
		A_root_pos = FindControlPoint1(prevPosture->root_pos, startPosture->root_pos, endPosture->root_pos);
		vector temp = Lerp(2.0, prevPosture->root_pos, startPosture->root_pos);
		B_root_pos = Lerp(1.0/3.0, endPosture->root_pos, temp);
		for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
			Quaternion<double> prevAngle; Euler2Quaternion(prevPosture->bone_rotation[bone].p, prevAngle);
			Quaternion<double> startAngle; Euler2Quaternion(startPosture->bone_rotation[bone].p, startAngle);
			Quaternion<double> endAngle; Euler2Quaternion(endPosture->bone_rotation[bone].p, endAngle);

			Quaternion<double> temp = Slerp(2.0, prevAngle, startAngle);
			A_bone[bone] = FindControlPoint1(prevAngle, startAngle, endAngle);
			B_bone[bone] = Slerp(1.0/3.0, endAngle, temp);
		}
	} else {
		Posture * prevPosture = pInputMotion->GetPosture(prevKeyframe);
		Posture * nextPosture = pInputMotion->GetPosture(nextKeyframe);
		A_root_pos = FindControlPoint1(prevPosture->root_pos, startPosture->root_pos, endPosture->root_pos);
		B_root_pos = FindControlPoint2(startPosture->root_pos, endPosture->root_pos, nextPosture->root_pos);
		for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
			Quaternion<double> prevAngle; Euler2Quaternion(prevPosture->bone_rotation[bone].p, prevAngle);
			Quaternion<double> startAngle; Euler2Quaternion(startPosture->bone_rotation[bone].p, startAngle);
			Quaternion<double> endAngle; Euler2Quaternion(endPosture->bone_rotation[bone].p, endAngle);
			Quaternion<double> nextAngle; Euler2Quaternion(nextPosture->bone_rotation[bone].p, nextAngle);

			A_bone[bone] = FindControlPoint1(prevAngle, startAngle, endAngle);
			B_bone[bone] = FindControlPoint2(startAngle, endAngle, nextAngle);
		}
	}
}


void Interpolator::BezierInterpolationEuler(Motion * pInputMotion, Motion * pOutputMotion, int N)
{
	  int inputLength = pInputMotion->GetNumFrames(); // frames are indexed 0, ..., inputLength-1
	  int startKeyframe = 0;
	  int endKeyframe = 0;

	  while (startKeyframe + N + 1 < inputLength)
	  {
			endKeyframe = startKeyframe + N + 1;
			Posture * startPosture = pInputMotion->GetPosture(startKeyframe);
			Posture * endPosture = pInputMotion->GetPosture(endKeyframe);
			// copy start and end keyframe
			pOutputMotion->SetPosture(startKeyframe, *startPosture);
			pOutputMotion->SetPosture(endKeyframe, *endPosture);

			vector A_root_pos;
			vector B_root_pos;
			vector A_bone[MAX_BONES_IN_ASF_FILE];
			vector B_bone[MAX_BONES_IN_ASF_FILE];

			// Find control points for Bazier interpolation
			FindControlPointsForBezierEulerInterpolation(startKeyframe, N, pInputMotion, A_root_pos, B_root_pos, A_bone, B_bone);

			// interpolate in between
			for(int frame=1; frame<=N; frame++)
			{
				Posture interpolatedPosture;
				double t = 1.0 * frame / (N+1);

				// interpolate root position
				interpolatedPosture.root_pos = DeCasteljauEuler(t, startPosture->root_pos, A_root_pos, B_root_pos, endPosture->root_pos);

				// interpolate bone rotations
				for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
					interpolatedPosture.bone_rotation[bone] = DeCasteljauEuler(t, startPosture->bone_rotation[bone], A_bone[bone], B_bone[bone], endPosture->bone_rotation[bone]);
				}
				pOutputMotion->SetPosture(startKeyframe + frame, interpolatedPosture);
			}
			startKeyframe = endKeyframe;
	  }

	  // For remaining frames copy pInputMotion to pOutputMotion
	  // No interpolation is required
	  for(int frame=startKeyframe+1; frame<inputLength; frame++)
	    pOutputMotion->SetPosture(frame, *(pInputMotion->GetPosture(frame)));

}

void Interpolator::LinearInterpolationQuaternion(Motion * pInputMotion, Motion * pOutputMotion, int N)
{
  // students should implement this
	  int inputLength = pInputMotion->GetNumFrames(); // frames are indexed 0, ..., inputLength-1

	  int startKeyframe = 0;
	  while (startKeyframe + N + 1 < inputLength)
	  {
		int endKeyframe = startKeyframe + N + 1;

		Posture * startPosture = pInputMotion->GetPosture(startKeyframe);
		Posture * endPosture = pInputMotion->GetPosture(endKeyframe);

		// copy start and end keyframe
		pOutputMotion->SetPosture(startKeyframe, *startPosture);
		pOutputMotion->SetPosture(endKeyframe, *endPosture);

		// interpolate in between
		for(int frame=1; frame<=N; frame++)
		{
		  Posture interpolatedPosture;
		  double t = 1.0 * frame / (N+1);

		  // interpolate root position using Linear Euler
		  interpolatedPosture.root_pos = startPosture->root_pos * (1-t) + endPosture->root_pos * t;

		  // interpolate bone rotations
		  for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
			  //interpolatedPosture.bone_rotation[bone] = startPosture->bone_rotation[bone] * (1-t) + endPosture->bone_rotation[bone] * t;
			  Quaternion<double> startBoneAngle;
			  Euler2Quaternion(startPosture->bone_rotation[bone].p, startBoneAngle);
			  Quaternion<double> endBoneAngle;
			  Euler2Quaternion(endPosture->bone_rotation[bone].p, endBoneAngle);
			  Quaternion<double> temp = Slerp(t, startBoneAngle, endBoneAngle);
			  Quaternion2Euler(temp, interpolatedPosture.bone_rotation[bone].p);

		  }
		  pOutputMotion->SetPosture(startKeyframe + frame, interpolatedPosture);
		}
		startKeyframe = endKeyframe;
	  }

	  for(int frame=startKeyframe+1; frame<inputLength; frame++)
	    pOutputMotion->SetPosture(frame, *(pInputMotion->GetPosture(frame)));
}

void Interpolator::BezierInterpolationQuaternion(Motion * pInputMotion, Motion * pOutputMotion, int N)
{
  // students should implement this
	int inputLength = pInputMotion->GetNumFrames(); // frames are indexed 0, ..., inputLength-1
	int startKeyframe = 0;
	int endKeyframe = 0;

	while (startKeyframe + N + 1 < inputLength)
	{
		endKeyframe = startKeyframe + N + 1;
		Posture * startPosture = pInputMotion->GetPosture(startKeyframe);
		Posture * endPosture = pInputMotion->GetPosture(endKeyframe);
		// copy start and end keyframe
		pOutputMotion->SetPosture(startKeyframe, *startPosture);
		pOutputMotion->SetPosture(endKeyframe, *endPosture);

		vector A_root_pos;
		vector B_root_pos;
		Quaternion<double> A_bone[MAX_BONES_IN_ASF_FILE];
		Quaternion<double> B_bone[MAX_BONES_IN_ASF_FILE];

		// Find control points for Bazier interpolation
		FindControlPointsForBezierQuaternionInterpolation(startKeyframe, N, pInputMotion, A_root_pos, B_root_pos, A_bone, B_bone);

		// interpolate in between
		for(int frame=1; frame<=N; frame++)
		{
			Posture interpolatedPosture;
			double t = 1.0 * frame / (N+1);

			// interpolate root position
			interpolatedPosture.root_pos = DeCasteljauEuler(t, startPosture->root_pos, A_root_pos, B_root_pos, endPosture->root_pos);

			// interpolate bone rotations
			for (int bone = 0; bone < MAX_BONES_IN_ASF_FILE; bone++) {
				Quaternion<double> startAngle; Euler2Quaternion(startPosture->bone_rotation[bone].p, startAngle);
				Quaternion<double> endAngle; Euler2Quaternion(endPosture->bone_rotation[bone].p, endAngle);
				Quaternion<double> interpolatedAngle = DeCasteljauQuaternion(t, startAngle, A_bone[bone], B_bone[bone], endAngle);
				Quaternion2Euler(interpolatedAngle, interpolatedPosture.bone_rotation[bone].p);
			}
			pOutputMotion->SetPosture(startKeyframe + frame, interpolatedPosture);
		}
		startKeyframe = endKeyframe;
	}

	// For remaining frames copy pInputMotion to pOutputMotion
	// No interpolation is required
	for(int frame=startKeyframe+1; frame<inputLength; frame++)
	pOutputMotion->SetPosture(frame, *(pInputMotion->GetPosture(frame)));

}

void Interpolator::Euler2Quaternion(double angles[3], Quaternion<double> & q) 
{
  // students should implement this
	double rotationMat[9];
	Euler2Rotation(angles, rotationMat);
	q = Quaternion<double>::Matrix2Quaternion(rotationMat);
	q.Normalize();
}

void Interpolator::Quaternion2Euler(Quaternion<double> & q, double angles[3]) 
{
  // students should implement this
	double rotationMat[9];
	q.Quaternion2Matrix(rotationMat);
	Rotation2Euler(rotationMat, angles);
}

Quaternion<double> Interpolator::Slerp(double t, Quaternion<double> & qStart, Quaternion<double> & qEnd_)
{
	// students should implement this
	Quaternion<double> result;
	double dotProduct = qStart.Gets() * qEnd_.Gets() + qStart.Getx() * qEnd_.Getx() +  qStart.Gety() * qEnd_.Gety() + qStart.Getz() * qEnd_.Getz();

	// Though the quaternions are having unit magnitude, their dot product sometimes exceeds [-1 1]
	// In such case the acos function returns NaN. To prevent this, we are truncating
	// the dot product to be in the range [-1 1]
	if (dotProduct > 1) dotProduct = 1;
	if (dotProduct < -1) dotProduct = -1;

	// Ref: Page 98 Computer Animation Algorithms and Techniques
	// If dot product of quaternions is positive then path from qStart to qEnd is shorter
	// otherwise path from qStart to -qEnd is shorter
	if (dotProduct < 0) {qEnd_ = -1 * qEnd_; dotProduct *= -1.0;}

	double theta = acos(dotProduct);

	//
	// For very small values of sin(theta) the slerp equation is returning NaN.
	// lim theta->0 sin((1-t)*theta)/sin(theta) = (1-t)
	// lim theta->0 sin(t*theta)/sin(theta) = t
	//
	if (abs(sin(theta)) < 0.0000001) {
		result = (1 - t) * qStart + t * qEnd_;
	} else {
		result = (sin((1 - t) * theta) / sin(theta)) * qStart + (sin(t * theta) / sin(theta)) * qEnd_;
	}

	result.Normalize();
  	return result;
}

Quaternion<double> Interpolator::Double(Quaternion<double> p, Quaternion<double> q)
{
  // students should implement this
  Quaternion<double> result;
  result = Slerp(2.0, p, q);
  return result;
}

vector Interpolator::DeCasteljauEuler(double t, vector p0, vector p1, vector p2, vector p3)
{
	// students should implement this
	vector result;
	vector q0 = Lerp(t, p0, p1);
	vector q1 = Lerp(t, p1, p2);
	vector q2 = Lerp(t, p2, p3);
	vector r0 = Lerp(t, q0, q1);
	vector r1 = Lerp(t, q1, q2);
	result = Lerp(t, r0, r1);
	return result;
}

Quaternion<double> Interpolator::DeCasteljauQuaternion(double t, Quaternion<double> p0, Quaternion<double> p1, Quaternion<double> p2, Quaternion<double> p3)
{
    // students should implement this
	Quaternion<double> result;
	Quaternion<double> q0 = Slerp(t, p0, p1);
	Quaternion<double> q1 = Slerp(t, p1, p2);
	Quaternion<double> q2 = Slerp(t, p2, p3);
	Quaternion<double> r0 = Slerp(t, q0, q1);
	Quaternion<double> r1 = Slerp(t, q1, q2);
	result = Slerp(t, r0, r1);
	return result;
}

