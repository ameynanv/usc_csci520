#ifdef WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>


using namespace std;

inline bool isInteger(const std::string & s)
{
   if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false ;
   char * p ;
   strtol(s.c_str(), &p, 10) ;
   return (*p == 0) ;
}

void split(string str, vector<string> &tokens) {
	istringstream iss(str);
	copy(istream_iterator<string>(iss),
	     istream_iterator<string>(),
	     back_inserter(tokens));
}

int main(int argc, char **argv) 
{
	string inputFileName;
	string outputFileName;
	if(argc != 3) {
		cout << "Usage extractFromAMC <input amc file> <output csv file>" << endl;
		return 0;
	}
	inputFileName = argv[1];
	outputFileName = argv[2];

	string line;
	ifstream inputAMCFile(inputFileName.c_str());
	ofstream outputCSVFile (outputFileName.c_str());
	if (!outputCSVFile.is_open())
		cout << "Unable to open output CSV file" << endl;

	if (inputAMCFile.is_open()) {
		int frameNumber = 0;
		while(getline(inputAMCFile, line)) {
			vector<string> tokens;
			if (isInteger(line)) {
			   frameNumber = atoi(line.c_str());
			   outputCSVFile << frameNumber;
			   outputCSVFile << ",";
			} else if (strcmp("root", line.substr(0,4).c_str()) == 0) {
				split(line, tokens);
				outputCSVFile << tokens[6];
				outputCSVFile << ",";
			} else if (strcmp("lfemur", line.substr(0,6).c_str()) == 0) {
				split(line, tokens);
				outputCSVFile << tokens[1];
				outputCSVFile << endl;
			}
		}
		inputAMCFile.close();
		outputCSVFile.close();
	} else {
		cout << "Unable to open AMC file ";
		cout << inputFileName.c_str() << endl;
	}
	return 0;
}

